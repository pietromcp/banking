using System.Threading.Tasks;

namespace Banking.Core.Holders {
    public interface HoldersRepository {
        Task Save(Holder holder);
    }
}