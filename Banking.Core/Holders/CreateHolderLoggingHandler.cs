using System;
using System.Threading.Tasks;
using Banking.Core.Support;

namespace Banking.Core.Holders {
    public class CreateHolderLoggingHandler : CommandHandler<CreateHolderCommand> {
        public Task Handle(CreateHolderCommand cmd) {
            Console.WriteLine($"Executing CreateHolderLoggingHandler for {cmd}");
            return Task.CompletedTask;
        }
    }
}