using System;
using System.Threading.Tasks;
using Banking.Core.Support;

namespace Banking.Core.Holders {
    public class CreateHolderUseCaseHandler : CommandHandler<CreateHolderCommand> {
        private readonly Clock clock;
        private readonly HolderIdGenerator idGenerator;
        private readonly HoldersRepository repo;

        public CreateHolderUseCaseHandler(Clock clock, HolderIdGenerator idGenerator, HoldersRepository repo) {
            this.clock = clock;
            this.repo = repo;
            this.idGenerator = idGenerator;
        }

        public async Task Handle(CreateHolderCommand cmd) {
            Holder newHolder = await ValidateHolder(cmd);
            await repo.Save(newHolder);
        }

        private async Task<Holder> ValidateHolder(CreateHolderCommand cmd) {
            if (string.IsNullOrEmpty(cmd.FirstName)) {
                throw new ArgumentException("Holder's firstName can't be empty");
            }
            if (string.IsNullOrEmpty(cmd.LastName)) {
                throw new ArgumentException("Holder's lastName can't be empty");
            }
            if (string.IsNullOrEmpty(cmd.TaxCode)) {
                throw new ArgumentException("Holder's taxCode can't be empty");
            }

            var id = await idGenerator.GenerateId();
            return new Holder(id, clock.Now, cmd.FirstName, cmd.LastName, cmd.TaxCode, cmd.EmailAddress);
        }
    }
}