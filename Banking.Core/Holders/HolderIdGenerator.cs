using System.Threading.Tasks;

namespace Banking.Core.Holders {
    public interface HolderIdGenerator {
        Task<HolderId> GenerateId();
    }
}