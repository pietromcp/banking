using Banking.Core.Support;

namespace Banking.Core.Holders {
    public record CreateHolderCommand : Command {
        public CreateHolderCommand(CommandId id) : base(id) {
        }

        public string FirstName { get; init; }
        public string LastName { get; init; }
        public string TaxCode { get; init; }
        public EmailAddress EmailAddress { get; init; }
    }
}