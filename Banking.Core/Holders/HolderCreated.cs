using System;
using Banking.Core.Support;

namespace Banking.Core.Holders {
    public record HolderCreated : AggregateEvent {
        public string FirstName { get; }
        public string LastName { get; }
        public string TaxCode { get; }
        public EmailAddress Email { get; }
        public HolderCreated(HolderId holderId, long holderVersion, DateTimeOffset occurredOn, string firstName, string lastName, string taxCode, EmailAddress email) :
            base(holderId, holderVersion, occurredOn) {
            FirstName = firstName;
            LastName = lastName;
            TaxCode = taxCode;
            Email = email;
        }
    }
}