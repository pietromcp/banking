using System;
using Banking.Core.Support;

namespace Banking.Core.Holders {
    public class Holder : EventSourcedAggregate {
        public HolderId Id { get; private set; }
        public DateTimeOffset CreatedAt { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string TaxCode { get; private set; }
        public EmailAddress Contact { get; private set; }

        public Holder(HolderId id, DateTimeOffset createdAt, string firstName, string lastName, string taxCode, EmailAddress contact) {
            ApplyAndPublish(new HolderCreated(id, InitialVersion, createdAt, firstName, lastName, taxCode, contact));
        }

        private void Apply(HolderCreated evt) {
            Id = (HolderId) evt.AggregateId;
            CreatedAt = evt.OccurredOn;
            FirstName = evt.FirstName;
            LastName = evt.LastName;
            TaxCode = evt.TaxCode;
            Contact = evt.Email;
        }
    }
}