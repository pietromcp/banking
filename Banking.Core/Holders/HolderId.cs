using Banking.Core.Support;

namespace Banking.Core.Holders {
    public record HolderId : AggregateId {
        public HolderId(string value) {
            Value = value;
        }

        public string Value { get; }
    }
}