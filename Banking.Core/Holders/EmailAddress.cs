using System;

namespace Banking.Core.Holders {
    public record EmailAddress {
        private EmailAddress(string value) {
            Value = value;
        }

        public string Value { get; }

        public static EmailAddress From(string value) {
            if (IsValid(value)) {
                return new EmailAddress(value);
            }
            throw new FormatException($"'{value}' is not a valid email address");
        }

        private static bool IsValid(string address) {
            return address.Contains('@');
        }
    }
}