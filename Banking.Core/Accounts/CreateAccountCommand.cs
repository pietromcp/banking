using System;
using System.Text.RegularExpressions;
using Banking.Core.Holders;
using Banking.Core.Support;

namespace Banking.Core.Accounts {
    public record CreateAccountCommand : Command {
        public CreateAccountCommand(CommandId id) : base(id) {
        }

        public AccountName Nickname { get; init; }
        public HolderId HolderId { get; init; }
    }

    public record AccountName {
        public string Value { get; private init; }

        private AccountName(string value) {
            Value = value;
        }

        public static AccountName From(string value) {
            if (new Regex("[a-z]+").Match(value).Success) {
                return new AccountName(value);
            }

            throw new ArgumentException($"{value} is not a valid AccountName");
        }
    }
}