using System.Threading.Tasks;

namespace Banking.Core.Accounts {
    public interface AccountsRepository {
        Task Save(Account account);

        Task<Account> Get(AccountId id);
    }
}