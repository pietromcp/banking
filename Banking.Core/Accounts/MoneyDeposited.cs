using System;
using Banking.Core.Support;

namespace Banking.Core.Accounts {
    public record MoneyDeposited : AggregateEvent {
        public decimal Amount { get; }

        public MoneyDeposited(AccountId id, in long version, in DateTimeOffset occurredOn, decimal amount) 
            : base(id, version, occurredOn) {
            Amount = amount;
        }
    }
    
    public record MoneyWithdrawn : AggregateEvent {
        public decimal Amount { get; }

        public MoneyWithdrawn(AccountId id, in long version, in DateTimeOffset occurredOn, decimal amount) 
            : base(id, version, occurredOn) {
            Amount = amount;
        }
    }
}