using System;
using Banking.Core.Holders;
using Banking.Core.Support;

namespace Banking.Core.Accounts {
    public class Account : EventSourcedAggregate {
        public static decimal InitialBalance = 0;
        
        public AccountId Id { get; private set; }
        public DateTimeOffset CreatedAt { get; private set;}
        public AccountName Name { get; private set; }
        public HolderId HolderId { get; private set;}
        public decimal Balance { get; private set; }


        public Account(AccountId id, DateTimeOffset createdAt, AccountName name, HolderId holderId) {
            ApplyAndPublish(new AccountCreated(id, InitialVersion, createdAt, name, holderId, InitialBalance));
        }

        public Account(params AggregateEvent[] events) : base(events) {
        }
        
        private void Apply(AccountCreated evt) {
            Id = (AccountId) evt.AggregateId;
            CreatedAt = evt.OccurredOn;
            Name = evt.AccountName;
            HolderId = evt.HolderId;
        }
        
        private void Apply(MoneyDeposited evt) {
            Balance += evt.Amount;
        }
        
        private void Apply(MoneyWithdrawn evt) {
            Balance -= evt.Amount;
        }

        public void Withdraw(decimal amount, DateTimeOffset when) {
            ApplyAndPublish(new MoneyWithdrawn(Id, Version + 1, when, amount));
        }
        
        public void Deposit(decimal amount, DateTimeOffset when) {
            ApplyAndPublish(new MoneyDeposited(Id, Version + 1, when, amount));
        }
    }
}