using System.Threading.Tasks;

namespace Banking.Core.Accounts {
    public interface AccountIdGenerator {
        Task<AccountId> GenerateId();
    }
}