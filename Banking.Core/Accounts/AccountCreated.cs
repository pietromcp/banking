using System;
using Banking.Core.Holders;
using Banking.Core.Support;

namespace Banking.Core.Accounts {
    public record AccountCreated : AggregateEvent {
        public AccountName AccountName { get; }
        public HolderId HolderId { get; }
        public decimal Balance { get; }

        public AccountCreated(AccountId id, in long version, in DateTimeOffset createdAt, AccountName accountName, HolderId holderId, decimal balance) 
            : base(id, version, createdAt) {
            AccountName = accountName;
            HolderId = holderId;
            Balance = balance;
        }
    }
}