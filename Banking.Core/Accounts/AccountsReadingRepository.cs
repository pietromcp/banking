using System.Threading.Tasks;

namespace Banking.Core.Accounts {
    public interface AccountsReadingRepository {
        Task<AccountId> FindAccountByName(AccountName cmdNickname);

        Task TrackAccountCreated(AccountCreated evt);
    }
}