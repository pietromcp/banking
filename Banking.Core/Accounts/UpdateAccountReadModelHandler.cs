using System.Threading.Tasks;
using Banking.Core.Support;

namespace Banking.Core.Accounts {
    public class UpdateAccountReadModelHandler : TypedEventHandler<AccountCreated> {
        private readonly AccountsReadingRepository repo;

        public UpdateAccountReadModelHandler(AccountsReadingRepository repo) {
            this.repo = repo;
        }

        public override Task HandleTyped(AccountCreated evt) {
            return repo.TrackAccountCreated(evt);
        }
    }
}