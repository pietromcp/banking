using Banking.Core.Support;

namespace Banking.Core.Accounts {
    public record AccountId : AggregateId {
        public AccountId(string value) {
            Value = value;
        }

        public string Value { get;  }
    }
}