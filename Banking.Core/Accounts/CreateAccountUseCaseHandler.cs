using System;
using System.Threading.Tasks;
using Banking.Core.Support;

namespace Banking.Core.Accounts {
    public class CreateAccountUseCaseHandler : CommandHandler<CreateAccountCommand> {
        private readonly Clock clock;
        private readonly AccountIdGenerator idGenerator;
        private readonly AccountsRepository repo;
        private readonly AccountsReadingRepository readingRepo;

        public CreateAccountUseCaseHandler(Clock clock, AccountIdGenerator idGenerator, AccountsRepository repo, AccountsReadingRepository readingRepo) {
            this.clock = clock;
            this.idGenerator = idGenerator;
            this.repo = repo;
            this.readingRepo = readingRepo;
        }

        public async Task Handle(CreateAccountCommand cmd) {
            Account newAccount = await ValidateAccount(cmd);
            await repo.Save(newAccount);
        }

        private async Task<Account> ValidateAccount(CreateAccountCommand cmd) {
            if (await readingRepo.FindAccountByName(cmd.Nickname) != null) {
                throw new ArgumentException($"An Account with nickname {cmd.Nickname.Value} already exists");
            }

            return new Account(await idGenerator.GenerateId(), clock.Now, cmd.Nickname, cmd.HolderId);
        }
    }
}