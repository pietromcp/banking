using System;

namespace Banking.Core {
    public interface Clock {
        public DateTimeOffset Now { get; }
    }
}