namespace Banking.Core.Support {
    public record Command {
        protected Command(CommandId id) {
            Id = id;
        }

        public CommandId Id { get; init; }
    }
}