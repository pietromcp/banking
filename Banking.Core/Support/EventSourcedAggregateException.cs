using System;

namespace Banking.Core.Support {
    internal class EventSourcedAggregateException : Exception {
        public EventSourcedAggregateException(string aggregate, string evt) : base($"Can't apply event '{evt}' to aggregate '{aggregate}'") {
        }
    }
}