using System;

namespace Banking.Core.Support {
    public record AggregateEvent {
        public AggregateEvent(AggregateId aggregateId, long aggregateVersion, DateTimeOffset occurredOn) {
            AggregateId = aggregateId;
            AggregateVersion = aggregateVersion;
            OccurredOn = occurredOn;
        }

        public AggregateId AggregateId { get; }
        public long AggregateVersion { get; }
        public DateTimeOffset OccurredOn { get; }
    }
    
    public record EventId {
        public string Value { get; }
    }

    public interface AggregateId {
        string Value { get; }
    }
}