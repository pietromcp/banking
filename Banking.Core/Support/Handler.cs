using System.Threading.Tasks;

namespace Banking.Core.Support {
    public interface Handler<in TReq> {
        Task Handle(TReq request);
    }
}