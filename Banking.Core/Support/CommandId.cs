namespace Banking.Core.Support {
    public record CommandId {
        public CommandId(string value) {
            Value = value;
        }

        public string Value { get; }
    }
}