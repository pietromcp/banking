namespace Banking.Core.Support {
    public interface CommandHandler<in TCmd> : Handler<TCmd> where TCmd : Command {
    }
}