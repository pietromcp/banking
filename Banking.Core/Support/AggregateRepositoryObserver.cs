using System.Collections.Generic;
using System.Threading.Tasks;

namespace Banking.Core.Support {
    public interface AggregateRepositoryObserver {
        Task NotifyAggregateEvents(IReadOnlyCollection<AggregateEvent> events);
    }
}