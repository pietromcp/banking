using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Banking.Core.Support {
    public class EventSourcedAggregate {
        public static long InitialVersion = 1;

        private IList<AggregateEvent> pendingEvents = new List<AggregateEvent>();

        public IReadOnlyList<AggregateEvent> PendingEvents => pendingEvents.ToList().AsReadOnly();

        public long Version { get; private set; }

        public long CommittedVersion { get; private set; }

        protected EventSourcedAggregate(params AggregateEvent[] events) {
            foreach (var evt in events) {
                Apply(evt);
            }

            Version = CommittedVersion = events.Length;
        }

        public void Commit() {
            pendingEvents.Clear();
            CommittedVersion = Version;
        }

        private void Apply(AggregateEvent evt) {
            var method = GetType().GetMethod("Apply", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null,
                new Type[] { evt.GetType() }, null);
            Console.WriteLine($"Applying event {evt.GetType().Name} to aggregate of type {this.GetType().Name}: method is {method}");
            if (method != null) {
                method.Invoke(this, new[] { evt });
            } else {
                throw new EventSourcedAggregateException(GetType().Name, evt.GetType().Name);
            }
        }

        protected void ApplyAndPublish(AggregateEvent evt) {
            Apply(evt);
            pendingEvents.Add(evt);
            Version++;
        }
    }
}