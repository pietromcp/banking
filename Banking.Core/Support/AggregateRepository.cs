using System.Collections.Generic;
using System.Threading.Tasks;

namespace Banking.Core.Support {
    public interface AggregateRepository {
        Task Save(StreamId streamId, EventSourcedAggregate aggregate, long? expectedVersion = null);

        Task<IReadOnlyList<AggregateEvent>> Get(StreamId streamId);
    }
}