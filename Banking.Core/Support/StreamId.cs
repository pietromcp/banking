namespace Banking.Core.Support {
    public record StreamId {
        public string AggregateType { get; }
        public string AggregateId { get; }

        public StreamId(string aggregateType, AggregateId aggregateId) {
            AggregateType = aggregateType;
            AggregateId = aggregateId.Value;
        }
    }
}