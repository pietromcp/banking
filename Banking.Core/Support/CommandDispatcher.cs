using System.Threading.Tasks;

namespace Banking.Core.Support {
    public interface CommandDispatcher {
        Task Dispatch<TCmd>(TCmd cmd) where TCmd: Command;
    }
}