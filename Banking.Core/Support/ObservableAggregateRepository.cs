namespace Banking.Core.Support {
    public interface ObservableAggregateRepository {
        void AddObserver(AggregateRepositoryObserver observer);
    }
}