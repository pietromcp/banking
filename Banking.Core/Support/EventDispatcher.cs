using System.Threading.Tasks;

namespace Banking.Core.Support {
    public interface EventDispatcher {
        Task Dispatch(AggregateEvent evt);
    }
}