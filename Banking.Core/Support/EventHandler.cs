using System.Threading.Tasks;

namespace Banking.Core.Support {
    public interface EventHandler {
        Task Handle(AggregateEvent evt);
    }
    
    public interface EventHandler<TEvt> : EventHandler where TEvt : AggregateEvent {
    }

    public abstract class TypedEventHandler<TEvt> : EventHandler<TEvt> where TEvt : AggregateEvent {
        public Task Handle(AggregateEvent evt) {
            return HandleTyped((TEvt) evt);
        }

        public abstract Task HandleTyped(TEvt evt);
    }
}