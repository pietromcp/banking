using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;
using Banking.Common;
using Banking.Core.Support;

namespace Banking.InMemorySupport.EventSourcing {
    public class InMemoryAggregateRepository : AggregateRepository, ObservableAggregateRepository {
        private readonly IDictionary<StreamId, IList<AggregateEvent>> aggregates =
            new Dictionary<StreamId, IList<AggregateEvent>>();

        private readonly ICollection<AggregateRepositoryObserver> observers;

        public InMemoryAggregateRepository() {
            observers = new List<AggregateRepositoryObserver>();
        }

        public Task<IReadOnlyList<AggregateEvent>> Get(StreamId streamId) {
            var events = aggregates.ContainsKey(streamId)
                ? aggregates[streamId].ToImmutableList()
                : ImmutableList<AggregateEvent>.Empty;
            return Task.FromResult((IReadOnlyList<AggregateEvent>) events);
        }

        public async Task Save(StreamId streamId, EventSourcedAggregate aggregate, long? expectedVersion = null) {
            var pendingEvents = aggregate.PendingEvents;
            if (expectedVersion.HasValue && expectedVersion.Value > 0) {
                var old = aggregates.GetOrDefault(streamId);
                if (old == null) {
                    throw new VersionNotMatchException();
                }

                if (old.Count != expectedVersion.Value) {
                    throw new VersionNotMatchException();
                }

                foreach (var pendingEvent in aggregate.PendingEvents) {
                    old.Add(pendingEvent);
                }
            }
            else {
                if (aggregates.ContainsKey(streamId)) {
                    throw new VersionNotMatchException();
                }

                aggregates[streamId] = new List<AggregateEvent>(aggregate.PendingEvents);
            }

            await NotifyEvents(pendingEvents);

            aggregate.Commit();
        }

        private async Task NotifyEvents(IReadOnlyList<AggregateEvent> pendingEvents) {
            foreach (var observer in observers) {
                await observer.NotifyAggregateEvents(pendingEvents);
            }
        }


        public void AddObserver(AggregateRepositoryObserver observer) {
            observers.Add(observer);
        }
    }

    public class VersionNotMatchException : Exception {
    }
}