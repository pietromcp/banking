using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Banking.Core.Accounts;

namespace Banking.InMemorySupport.Accounts {
    public class InMemoryAccountsReadingRepository : AccountsReadingRepository {
        private readonly IDictionary<string, string> ByName = new Dictionary<string, string>();

        public InMemoryAccountsReadingRepository() {
            Console.WriteLine("Created InMemoryAccountsReadingRepository");
        }

        public Task<AccountId> FindAccountByName(AccountName cmdNickname) {
            return ByName.ContainsKey(cmdNickname.Value)
                ? Task.FromResult(new AccountId(ByName[cmdNickname.Value])) : Task.FromResult(null as AccountId);
        }

        public Task TrackAccountCreated(AccountCreated evt) {
            ByName[evt.AccountName.Value] = evt.AggregateId.Value;
            return Task.CompletedTask;
        }
    }
}