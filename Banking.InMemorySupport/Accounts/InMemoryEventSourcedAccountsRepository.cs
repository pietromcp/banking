using System.Linq;
using System.Threading.Tasks;
using Banking.Core.Accounts;
using Banking.Core.Support;

namespace Banking.InMemorySupport.Accounts {
    public class InMemoryEventSourcedAccountsRepository : AccountsRepository {
        private readonly AggregateRepository repo;

        public InMemoryEventSourcedAccountsRepository(AggregateRepository repo) {
            this.repo = repo;
        }

        public Task Save(Account account) {
            var streamId = StreamId(account.Id);
            return repo.Save(streamId, account, account.CommittedVersion);
        }

        public async Task<Account> Get(AccountId id) {
            var events = await repo.Get(StreamId(id));
            if (events.Any()) {
                return new Account(events.ToArray());
            } else {
                return null;
            }
        }

        private StreamId StreamId(AccountId id) => new StreamId("Account", id);
    }
}