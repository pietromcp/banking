using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Banking.Core.Holders;

namespace Banking.InMemorySupport.Holders {
    public class InMemoryHoldersRepository : HoldersRepository {
        private readonly IDictionary<HolderId, InMemoryHolder> data;

        public InMemoryHoldersRepository() {
            data = new Dictionary<HolderId, InMemoryHolder>();
        }

        public Task Save(Holder holder) {
            var persistentHolder = InMemoryHolder.From(holder);
            data[holder.Id] = persistentHolder;
            Console.WriteLine($"Saved Holder: {persistentHolder}");
            return Task.CompletedTask;
        }
    }

    internal record InMemoryHolder {
        public string Id { get; init; }
        public string FirstName { get; init; }
        public string LastName { get; init; }
        public string TaxCode { get; init; }
        public string EmailAddress { get; init; }
        
        public static InMemoryHolder From(Holder holder) {
            return new InMemoryHolder {
                Id = holder.Id.Value,
                FirstName = holder.FirstName,
                LastName = holder.LastName,
                TaxCode = holder.TaxCode,
                EmailAddress = holder.Contact.Value
            };
        }
    }
}