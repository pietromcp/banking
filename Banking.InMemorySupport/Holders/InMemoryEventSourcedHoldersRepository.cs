using System.Threading.Tasks;
using Banking.Core.Holders;
using Banking.Core.Support;

namespace Banking.InMemorySupport.Holders {
    public class InMemoryEventSourcedHoldersRepository : HoldersRepository {
        private readonly AggregateRepository repo;

        public InMemoryEventSourcedHoldersRepository(AggregateRepository repo) {
            this.repo = repo;
        }

        public Task Save(Holder holder) {
            var streamId = new StreamId("Holder", holder.Id);
            return repo.Save(streamId, holder, holder.CommittedVersion);
        }
    }
}