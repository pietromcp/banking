using System;
using System.Collections.Generic;

namespace Banking.InMemorySupport.Dispatching {
    public class InMemoryDispatcherConfiguration {
        public InMemoryDispatcherConfiguration(IDictionary<Type, IEnumerable<object>> handlers) {
            Handlers = handlers;
        }

        public IDictionary<Type, IEnumerable<object>> Handlers { get; }
    }
}