using System;
using System.Collections.Generic;

namespace Banking.InMemorySupport.Dispatching {
    public class InMemoryCommandDispatcherConfiguration : InMemoryDispatcherConfiguration {
        public InMemoryCommandDispatcherConfiguration(IDictionary<Type, IEnumerable<object>> handlers) : base(handlers) {
        }
    }
}