using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Banking.Core.Support;
using EventHandler = Banking.Core.Support.EventHandler;

namespace Banking.InMemorySupport.Dispatching {
    public class InMemoryEventDispatcher : EventDispatcher, AggregateRepositoryObserver {
        private static readonly Random Randomizer = new Random();

        private static TimeSpan RandomDelay() {
            return TimeSpan.FromMilliseconds(Randomizer.Next(600));
        }

        private IDictionary<Type, IEnumerable<object>> handlers;

        public InMemoryEventDispatcher(InMemoryEventDispatcherConfiguration config) {
            handlers = config.Handlers;
        }
        
        public Task Dispatch(AggregateEvent evt) {
            var concreteHandlers = handlers
                .Where(h => CheckType(h, evt))
                .SelectMany(h => h.Value)
                .ToArray();
            
            if (concreteHandlers.Any()) {
                Parallel.ForEach(concreteHandlers, async (concreteHandler) => {
                    await Task.Delay(RandomDelay());
                    if (concreteHandler is EventHandler handler) {
                        try {
                            await handler.Handle(evt);
                        } catch (Exception ex) {
                            Console.WriteLine($"ERROR {ex.Message} {ex.StackTrace}");
                        }
                    }
                });
            } else {
                Console.WriteLine($"No handlers registered for event type '{evt.GetType().Name}'");
            }
            return Task.CompletedTask;
        }

        private bool CheckType(KeyValuePair<Type, IEnumerable<object>> h, AggregateEvent evt) {
            return h.Key.GetGenericArguments()[0].IsInstanceOfType(evt);
        }

        public async Task NotifyAggregateEvents(IReadOnlyCollection<AggregateEvent> events) {
            foreach (var evt in events) {
                await Dispatch(evt);
            }
        }
    }
}