using System;
using System.Collections.Generic;

namespace Banking.InMemorySupport.Dispatching {
    public class InMemoryEventDispatcherConfiguration : InMemoryDispatcherConfiguration {
        public InMemoryEventDispatcherConfiguration(IDictionary<Type, IEnumerable<object>> handlers) : base(handlers) {
        }
    }
}