using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Banking.Common;
using Banking.Core.Support;

namespace Banking.InMemorySupport.Dispatching {
    public class InMemoryCommandDispatcher : InMemoryDispatcher, CommandDispatcher {
        private static readonly Random Randomizer = new Random();

        private static TimeSpan RandomDelay() {
            return TimeSpan.FromMilliseconds(Randomizer.Next(600));
        }

        private IDictionary<Type, IEnumerable<object>> handlers;

        public InMemoryCommandDispatcher(InMemoryCommandDispatcherConfiguration config) {
            handlers = config.Handlers;
        }
        
        protected Task DispatchInternal<TReq>(TReq cmd) {
            Type handlerType = BaseHandlerType.MakeGenericType(cmd.GetType());
            var concreteHandlers = handlers.GetOrDefault(handlerType);
            if (concreteHandlers == null) {
                Console.WriteLine($"No handlers registered for command type '{cmd.GetType().Name}'");
            }

            Parallel.ForEach(concreteHandlers, async (concreteHandler) => {
                await Task.Delay(RandomDelay());
                if (concreteHandler is Handler<TReq> handler) {
                    try {
                        await handler.Handle(cmd);
                    } catch (Exception ex) {
                        Console.WriteLine($"ERROR {ex.Message} {ex.StackTrace}");
                    }
                }
            });
            return Task.CompletedTask;
        }

        public Task Dispatch<TCmd>(TCmd cmd) where TCmd : Command {
            return DispatchInternal(cmd);
        }

        private Type BaseHandlerType { get; } = typeof(CommandHandler<>);
    }
}