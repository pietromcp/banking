using System.Linq;
using Banking.Core.Support;
using Banking.InMemorySupport.Dispatching;
using Microsoft.Extensions.DependencyInjection;

namespace Banking.Boot {
    internal static class IoCExtensions {
        public static void AddInMemoryDispatchingStuff(this IServiceCollection services) {
            services.AddSingleton((provider) => new InMemoryCommandDispatcherConfiguration(services
                .Where(t => t.ServiceType.IsGenericType &&
                            t.ServiceType.GetGenericTypeDefinition() == typeof(CommandHandler<>))
                .GroupBy(sd => sd.ServiceType)
                .ToDictionary(g => g.Key, g=>provider.GetServices(g.Key))));
            services.AddSingleton<CommandDispatcher, InMemoryCommandDispatcher>();
            
            services.AddSingleton((provider) => new InMemoryEventDispatcherConfiguration(services
                .Where(t => t.ServiceType.IsGenericType &&
                            t.ServiceType.GetGenericTypeDefinition() == typeof(Core.Support.EventHandler<>))
                .GroupBy(sd => sd.ServiceType)
                .ToDictionary(g => g.Key, g=>provider.GetServices(g.Key))));
            services.AddSingleton<InMemoryEventDispatcher>();
            services.AddSingleton<EventDispatcher>(provider => provider.GetRequiredService<InMemoryEventDispatcher>());
        }
    }
}