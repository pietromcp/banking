using System;
using System.Threading.Tasks;
using Banking.Core.Support;

namespace Banking.Boot {
    internal class LogAggregateHandlerInConsole : Core.Support.EventHandler<AggregateEvent> {
        public Task Handle(AggregateEvent evt) {
            Console.WriteLine($"DEBUG Raised event {evt}");
            return Task.CompletedTask;
        }
    }
}