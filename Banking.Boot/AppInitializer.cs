using Banking.Core.Support;
using Banking.InMemorySupport.Dispatching;

namespace Banking.Boot {
    public class AppInitializer {
        private readonly InMemoryEventDispatcher eventDispatcher;
        private readonly ObservableAggregateRepository aggregateRepo;

        public AppInitializer(InMemoryEventDispatcher eventDispatcher, ObservableAggregateRepository aggregateRepo) {
            this.eventDispatcher = eventDispatcher;
            this.aggregateRepo = aggregateRepo;
        }

        internal void Initialize() {
            aggregateRepo.AddObserver(eventDispatcher);
        }
    }
}