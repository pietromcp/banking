using Banking.Core;
using Banking.Core.Accounts;
using Banking.Core.Holders;
using Banking.Core.Support;
using Banking.InMemorySupport.Accounts;
using Banking.InMemorySupport.EventSourcing;
using Banking.InMemorySupport.Holders;
using Banking.SystemAdapters.Accounts;
using Banking.SystemAdapters.Holders;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Banking.Boot {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddSingleton<Clock, SystemClock>();
            services.AddSingleton<CommandHandler<CreateHolderCommand>, CreateHolderUseCaseHandler>();
            services.AddSingleton<CommandHandler<CreateHolderCommand>, CreateHolderLoggingHandler>();
            services.AddSingleton<CommandHandler<CreateAccountCommand>, CreateAccountUseCaseHandler>();
            
            services.AddSingleton<Core.Support.EventHandler<AccountCreated>, UpdateAccountReadModelHandler>();
            services.AddSingleton<Core.Support.EventHandler<AggregateEvent>, LogAggregateHandlerInConsole>();
            // services.AddSingleton<HoldersRepository, InMemoryHoldersRepository>();            
            services.AddSingleton<HoldersRepository, InMemoryEventSourcedHoldersRepository>();            
            services.AddSingleton<HolderIdGenerator, GuidBasedHolderIdGenerator>();            
            services.AddSingleton<InMemoryAggregateRepository>();
            services.AddSingleton<AggregateRepository>(provider => provider.GetRequiredService<InMemoryAggregateRepository>());
            services.AddSingleton<ObservableAggregateRepository>(provider => provider.GetRequiredService<InMemoryAggregateRepository>());

            services.AddSingleton<AccountsReadingRepository, InMemoryAccountsReadingRepository>();
            services.AddSingleton<AccountsRepository, InMemoryEventSourcedAccountsRepository>();
            services.AddSingleton<AccountIdGenerator, GuidBasedAccountIdGenerator>();

            services.AddInMemoryDispatchingStuff();

            services.AddSingleton<AppInitializer>();
            
            services.AddControllers();
            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Banking.Boot", Version = "v1"});
            });
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AppInitializer initializer) {
            initializer.Initialize();
            
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Banking.Boot v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}