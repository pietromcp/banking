using System;
using Banking.Core;

namespace Banking.Boot {
    public class SystemClock : Clock {
        public DateTimeOffset Now => DateTimeOffset.Now;
    }
}