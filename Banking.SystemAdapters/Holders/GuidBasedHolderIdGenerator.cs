using System;
using System.Threading.Tasks;
using Banking.Core.Holders;

namespace Banking.SystemAdapters.Holders {
    public class GuidBasedHolderIdGenerator : HolderIdGenerator {
        public Task<HolderId> GenerateId() {
            return Task.FromResult(new HolderId(Guid.NewGuid().ToString()));
        }
    }
}