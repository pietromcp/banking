using System;
using System.Threading.Tasks;
using Banking.Core.Accounts;

namespace Banking.SystemAdapters.Accounts {
    public class GuidBasedAccountIdGenerator : AccountIdGenerator {
        public Task<AccountId> GenerateId() {
            return Task.FromResult(new AccountId(Guid.NewGuid().ToString()));
        }
    }
}