using System;
using System.Threading.Tasks;
using Banking.Core.Holders;
using Banking.Core.Support;
using Microsoft.AspNetCore.Mvc;

namespace Banking.Api.Holders {
    [Route("holders")]
    public class HolderController : ControllerBase {
        private readonly CommandDispatcher dispatcher;

        public HolderController(CommandDispatcher dispatcher) {
            this.dispatcher = dispatcher;
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateHolder(CreateHolderInput input) {
            var commandId = new CommandId(Guid.NewGuid().ToString());
            var command = new CreateHolderCommand(commandId) {
                FirstName = input.FirstName,
                LastName = input.LastName,
                TaxCode = input.TaxCode,
                EmailAddress = EmailAddress.From(input.Email)
            };
            await dispatcher.Dispatch(command);
            return Accepted(new {commandId });
        }
    }

    public record CreateHolderInput {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TaxCode { get; set; }
        public string Email { get; set; }
    }
}