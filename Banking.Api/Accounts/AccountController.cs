using System;
using System.Threading.Tasks;
using Banking.Core.Accounts;
using Banking.Core.Holders;
using Banking.Core.Support;
using Microsoft.AspNetCore.Mvc;

namespace Banking.Api.Accounts {
    [Route("accounts")]
    public class AccountController : ControllerBase {
        private readonly CommandDispatcher dispatcher;

        public AccountController(CommandDispatcher dispatcher) {
            this.dispatcher = dispatcher;
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateHolder(CreateAccountInput input) {
            var commandId = new CommandId(Guid.NewGuid().ToString());
            var command = new CreateAccountCommand(commandId) {
                Nickname = AccountName.From(input.Nickname),
                HolderId = new HolderId(input.HolderId)
            };
            await dispatcher.Dispatch(command);
            return Accepted(new { commandId });
        }
    }

    public class CreateAccountInput {
        public string Nickname { get; set; }
        public string HolderId { get; set; }
    }
}