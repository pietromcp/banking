using System;
using System.Collections.Generic;
using System.Linq;

namespace Banking.Common {
    public static class DictionaryExtensions {
        public static TValue GetOr<TKey, TValue>(this IDictionary<TKey, TValue> self, TKey key, Func<TKey, TValue> defaultProvider) {
            return self.ContainsKey(key) ? self[key] : defaultProvider(key);
        }
        
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> self, TKey key) {
            return self.ContainsKey(key) ? self[key] : default(TValue);
        }
    }

    public static class EnumerableExtensions {
        public static IEnumerable<TItem> OrEmpty<TItem>(this IEnumerable<TItem> self) {
            return self ?? Enumerable.Empty<TItem>();
        }
    }
}