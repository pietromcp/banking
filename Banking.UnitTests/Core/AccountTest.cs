using System;
using System.Linq;
using Banking.Core.Accounts;
using Banking.Core.Holders;
using Banking.Core.Support;
using NHamcrest;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;

namespace Banking.UnitTests.Core {
    public class AccountTest {
        [Fact]
        public void WithdrawMoneyShouldPublishAnEvent() {
            var id = new AccountId("acc0");
            var account = new Account(
                new AccountCreated(id, EventSourcedAggregate.InitialVersion, DateTimeOffset.Now, AccountName.From("alias0"), new HolderId("holder0"), Account.InitialBalance),
                new MoneyDeposited(id, 2, DateTimeOffset.Now, 200M)
            );
            var amount = 100M;
            var when = DateTimeOffset.Now;
            account.Withdraw(amount, when);
            Assert.That(account.PendingEvents, Is.OfLength<AggregateEvent>(1));
            Assert.That(account.PendingEvents.Single(), 
                Is.OfType<AggregateEvent, MoneyWithdrawn>(Is.EqualTo(new MoneyWithdrawn(id, 3, when, amount))));
        }
        
        [Fact]
        public void WithdrawMoneyShouldReduceTheBalance() {
            var id = new AccountId("acc0");
            var account = new Account(
                new AccountCreated(id, EventSourcedAggregate.InitialVersion, DateTimeOffset.Now, AccountName.From("alias0"), new HolderId("holder0"), Account.InitialBalance),
                new MoneyDeposited(id, 2, DateTimeOffset.Now, 200M)
            );
            var amount = 100M;
            var when = DateTimeOffset.Now;
            account.Withdraw(amount, when);
            Assert.That(account.Balance, Is.EqualTo(100M));
        }
        
        [Fact]
        public void DepositMoneyShouldPublishAnEvent() {
            var id = new AccountId("acc0");
            var account = new Account(
                new AccountCreated(id, EventSourcedAggregate.InitialVersion, DateTimeOffset.Now, AccountName.From("alias0"), new HolderId("holder0"), Account.InitialBalance)
            );
            var amount = 100M;
            var when = DateTimeOffset.Now;
            account.Deposit(amount, when);
            Assert.That(account.PendingEvents, Is.OfLength<AggregateEvent>(1));
            Assert.That(account.PendingEvents.Single(), 
                Is.OfType<AggregateEvent, MoneyDeposited>(Is.EqualTo(new MoneyDeposited(id, 2, when, amount))));
        }
        
        [Fact]
        public void DepositMoneyShouldAugmentTheBalance() {
            var id = new AccountId("acc0");
            var account = new Account(
                new AccountCreated(id, EventSourcedAggregate.InitialVersion, DateTimeOffset.Now, AccountName.From("alias0"), new HolderId("holder0"), Account.InitialBalance),
                new MoneyDeposited(id, 2, DateTimeOffset.Now, 200M)
            );
            var amount = 100M;
            var when = DateTimeOffset.Now;
            account.Deposit(amount, when);
            Assert.That(account.Balance, Is.EqualTo(300M));
        }
    }
}